<?php
// Koneksi Database
include('dbconnect.php');

//Tampilkan Data yang akan diedit
$id = $_GET['id'];
$query = mysqli_query($koneksi, "SELECT * FROM tb_mahasiswa WHERE id_mhs = '$id' ");
$data = mysqli_fetch_array($query);
if($data)
{
  //Jika data ditemukan, maka data ditampung ke dalam variabel
  $vnim = $data['nim'];
  $vnama = $data['nama'];
  $vprodi = $data['prodi'];
  $vtahun = $data['tahun'];
}

// Edit Data
if(isset($_POST['simpan'])) {
  $edit = mysqli_query($koneksi, "UPDATE tb_mahasiswa set
											 	nim = '$_POST[nim]',
											 	nama = '$_POST[nama]',
											 	prodi = '$_POST[prodi]',
												tahun = '$_POST[tahun]' WHERE id_mhs = '$_GET[id]'
										  ");
  if($edit) //jika edit sukses
  {
    echo "<script>
            alert('Edit Data Mahasiswa Berhasil');
            document.location='index.php';
          </script>";
  }
  else
  {
    echo "<script>
            alert('Edit Data Mahasiswa Gagal');
            document.location='index.php';
          </script>";
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<title>Database Mahasiswa</title>
</head>
<body>
  <nav class="navbar navbar-dark bg-dark">
		<div class="container">
			<span class="navbar-brand mb-0 h1">Database Mahasiswa</span>
		</div>
	</nav>
  <div class="container">
  <div class="row">
    <div class="col-sm-8">
      <!-- Awal Card Form -->
      <div class="card mt-3">
        <div class="card-header bg-primary text-white">
          Edit Data Mahasiswa
        </div>
        <div class="card-body">
          <form method="post" action="">
            <div class="form-group">
              <label>Nim</label>
              <input type="number" name="nim" value="<?=@$vnim?>" class="form-control" placeholder="Nomor Registrasi" required>
            </div>
            <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama" value="<?=@$vnama?>" class="form-control" placeholder="Nama Lengkap" required>
            </div>
            <div class="form-group">
              <label>Program Studi</label>
              <input type="text" name="prodi" value="<?=@$vprodi?>" class="form-control" placeholder="Prodi" required>
            </div>
            <div class="form-group">
              <label>Tahun Masuk</label>
              <input type="number" name="tahun" value="<?=@$vtahun?>" class="form-control" placeholder="Tahun Masuk" required>
            </div>
              <button type="submit" class="btn btn-success" name="simpan">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- jQuery and Bootstrap Bundle (includes Popper) -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body>
</html>
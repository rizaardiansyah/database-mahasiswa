<?php
	// Koneksi Database
	include('dbconnect.php');
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<title>Database Mahasiswa</title>
</head>
<body>
	<nav class="navbar navbar-dark bg-dark">
		<div class="container">
			<span class="navbar-brand mb-0 h1">Database Mahasiswa</span>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-4">
			<!-- Awal Card Form -->
				<div class="card mt-3">
					<div class="card-header bg-primary text-white">
						Form Input Mahasiswa Baru
					</div>
					<div class="card-body">
						<form method="post" action="tambah.php">
							<div class="form-group">
								<label>Nomor Registrasi</label>
								<input type="number" name="nim" value="<?=@$vnim?>" class="form-control" placeholder="Nomor Registrasi" autocomplete="off" required>
							</div>
							<div class="form-group">
								<label>Nama</label>
								<input type="text" name="nama" value="<?=@$vnama?>" class="form-control" placeholder="Nama Lengkap" autocomplete="off" required>
							</div>
							<div class="form-group">
								<label>Program Studi</label>
								<input type="text" name="prodi" value="<?=@$vprodi?>" class="form-control" placeholder="Prodi" autocomplete="off" required>
							</div>
							<div class="form-group">
								<label>Tahun Masuk</label>
								<input type="number" name="tahun" value="<?=@$vtahun?>" class="form-control" placeholder="Tahun Masuk" autocomplete="off" required>
							</div>
								<button type="submit" class="btn btn-success" name="simpan">Simpan</button>
						</form>
					</div>
				</div>
			<!-- Akhir Card Form -->
			</div>
			<div class="col-sm-12 col-md-8">
				<!-- Awal Card Tabel -->
				<div class="card mt-3">
					<div class="card-header bg-success text-white">
						Daftar Mahasiswa
					</div>
						<div class="card-body">
							<table class="table table-bordered table-striped">
								<tr>
									<th>No.</th>
									<th>Nim</th>
									<th>Nama</th>
									<th>Program Studi</th>
									<th>Tahun</th>
									<th>Aksi</th>
								</tr>
								<?php
									// Query Get All Mahasiswa
									$no = 1;
									$query = mysqli_query($koneksi, "SELECT * from tb_mahasiswa order by nim asc");
									while($data = mysqli_fetch_array($query)) :
								?>
								<tr>
									<td><?=$no++;?></td>
									<td><?=$data['nim']?></td>
									<td><?=$data['nama']?></td>
									<td><?=$data['prodi']?></td>
									<td><?=$data['tahun']?></td>
									<td>
										<a href="edit.php?id=<?=$data['id_mhs']?>" class="btn btn-warning"> Edit </a>
										<a href="hapus.php?id=<?=$data['id_mhs']?>" 
											onclick="return confirm('Apakah yakin ingin menghapus data ini?')" class="btn btn-danger"> Hapus </a>
									</td>
								</tr>
							<?php endwhile; //penutup perulangan while ?>
							</table>
						</div>
					</div>
				<!-- Akhir Card Tabel -->
			</div>
		</div>
	</div>

	<!-- jQuery and Bootstrap Bundle (includes Popper) -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body>
</html>